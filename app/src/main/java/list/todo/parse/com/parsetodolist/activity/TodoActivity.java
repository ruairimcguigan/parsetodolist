package list.todo.parse.com.parsetodolist.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import list.todo.parse.com.parsetodolist.R;
import list.todo.parse.com.parsetodolist.adapter.TaskAdapter;
import list.todo.parse.com.parsetodolist.model.Task;

public class TodoActivity extends AppCompatActivity {
    Button mSubmitButton;
    EditText mTaskInput;
    RecyclerView mTaskList;
    TaskAdapter mAdapter;

    final List<Task> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        updateData();
        initViews();
        initRecyclerView();
    }

    private void initViews() {
        mSubmitButton = (Button)findViewById(R.id.submit_button);
        mTaskInput = (EditText)findViewById(R.id.task_input_text);

    }

    private void initRecyclerView() {
        mTaskList = (RecyclerView)findViewById(R.id.task_list);
        mTaskList.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new TaskAdapter(getApplicationContext(), data);
        mTaskList.setAdapter(mAdapter);
    }

    public void createTask(View v){
        if (mTaskInput.getText().length() > 0){
            Task t = new Task();
            t.setDescription(mTaskInput.getText().toString());
            t.setCompleted(false);
            t.saveEventually();
            mTaskInput.setText("");
        }
    }

    private void updateData() {
        ParseQuery<Task> query = ParseQuery.getQuery(Task.class);
        query.findInBackground(new FindCallback<Task>() {
            @Override
            public void done(List<Task> tasks, ParseException error) {
                if(tasks != null){
                    for (ParseObject resultList : tasks) {
                        Task description = new Task();

                        description.setDescription(resultList.getString("description"));
                        data.add(description);
                    }
                }
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
