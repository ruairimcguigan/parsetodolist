package list.todo.parse.com.parsetodolist.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import list.todo.parse.com.parsetodolist.R;
import list.todo.parse.com.parsetodolist.model.Task;

/**
 * Created by rmcguigan on 03/01/16.
 */
public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.MyViewHolder>{

    List<Task> dataList = Collections.emptyList();
    private LayoutInflater inflater;

    public TaskAdapter (Context context, List<Task> data){
        inflater = LayoutInflater.from(context);
        dataList= data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(TaskAdapter.MyViewHolder holder, int position) {

        Task current = dataList.get(position);
        holder.mTaskListItem.setText(current.getDescription());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mTaskListItem;
        public MyViewHolder(View itemView) {
            super(itemView);

            mTaskListItem = (TextView) itemView.findViewById(R.id.list_item_text);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
