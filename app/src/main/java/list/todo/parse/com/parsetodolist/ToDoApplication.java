package list.todo.parse.com.parsetodolist;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseObject;

import list.todo.parse.com.parsetodolist.model.Task;

/**
 * Created by rmcguigan on 03/01/16.
 */
public class ToDoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "RLYz8rnQu5yB5XDboXH7r2rS4rasggXcLLUH3YW2","bk6SmAWUGBeFSO2vaLW00q4JvtLbyXK1QwLjgItB");
        Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG);

        // This tells Parse to use the annotation that we declared at the top of the model.
        ParseObject.registerSubclass(Task.class);
    }



}
